import numpy as np
from scipy.special import comb
import logging

logging.basicConfig(filename='logs.txt',level=logging.DEBUG)

GENERATIONS = 10
MINIMUM_PARENTS = 3
MINIMUM_POPULATION_SIZE = 7
CONSTRAINTS = 15# weight constraint
UNIQUE_GENES = True# repeat objects possible? | True/False
STATIC_GENOME_SIZE = False# size of genome | False or 1 <= int(STATIC_GENOME_SIZE) <= Inf if UNIQUE_GENES is False else objects_in_bag
MUTATION_RATE = 0.1# fraction | 0 <= MUTATION_RATE <= 1


class Population:
    """
    The population class stores a list of objects of the Chromosome class.

    Options are available to control the activity of a population:
        the_objects: the available items | a two column array
        minimum_population_size: minimum size of the population | int
        minimum_parents: minimum number of parents for the next generation | int
        constraints: weight constraint for the selection of items | int

    Populations pass control flags to chomosome sub-objects:
        unique_genes ensures genomes have no replicate genes | True/False
        static_genome_size allows the genome size to be controlled | An int or False.

    """
    def __init__(self, the_objects, minimum_population_size, minimum_parents, constraints, unique_genes, static_genome_size, mutation_rate):
        self._the_objects = the_objects

        self.minimum_population_size = minimum_population_size
        self.minimum_parents = minimum_parents
        self.constraints = constraints

        self.unique_genes = unique_genes
        self.static_genome_size = static_genome_size

        self.mutation_rate = mutation_rate

        self.populate()
        self.assess_population()

    def populate(self, genetics=False):
        """
        Generates a random population.
        """
        if genetics is False:
            self.current_generation = np.asarray(
                [self.generate_chromosome(inherited_genetics=None) for _ in range(self.minimum_population_size)])

    def iterate(self):
        """
        Takes the previous generation, populates a new one (including mutation), then assesses its component chromosomes.
        """
        the_ancestors = self.fitness >= np.random.uniform(low=0, high=1, size=len(self.current_generation))

        i = 0
        while np.count_nonzero(the_ancestors) < self.minimum_parents:
            the_ancestors[i] = True
            i += 1

        new_generation = np.concatenate((self.current_generation[the_ancestors], np.full(int(comb(np.count_nonzero(the_ancestors), 2)), 0)))
        the_parents = [0, 1]
        chromosome = np.count_nonzero(the_ancestors)
        while chromosome < len(new_generation):
            if the_ancestors[the_parents[0]] is False or the_parents[1] == len(the_ancestors):
                the_parents[0] += 1
                the_parents[1] = the_parents[0] + 1
            else:
                if the_ancestors[the_parents[1]]:
                    new_generation[chromosome] = self.generate_chromosome(self.frankenstein(self.population_index, the_parents, self.fitness))
                    chromosome += 1
                else:
                    the_parents[1] += 1

        if len(new_generation) < self.minimum_population_size:
            self.current_generation = np.concatenate((new_generation, np.asarray([self.generate_chromosome(inherited_genetics=None) for _ in range(self.minimum_population_size - len(new_generation))])))

        for chromosome in self.current_generation[2:]:
            if self.mutation_rate >= np.random.uniform(low=0, high=1):
                chromosome.mutate()

        self.assess_population()

    def generate_chromosome(self, inherited_genetics):
        """
        This is a dummy function to cleanly call the Chromosome constructor.
        """
        return Chromosome(
            self._the_objects,
            unique_genes=self.unique_genes,
            inherited_genetics=inherited_genetics,
            static_genome_size=self.static_genome_size)

    def assess_population(self):
        """
        Assesses the population, creates a pair of lists which order the current population by their evaluation scores.
        """
        evaluation = np.zeros((len(self.current_generation),))
        for i in range(len(self.current_generation)):
            evaluation[i] = self.do_evaluation(self.current_generation[i].get_values(self.constraints))
        if np.count_nonzero(evaluation) == 0:
            self.population_index = np.arange(len(self.current_generation))
            self.fitness = evaluation
            self.fitness[:self.minimum_parents - 1] = 1
        else:
            the_result = np.vstack((np.arange(len(self.current_generation)),
                                    (evaluation - min(evaluation)) / (max(evaluation) - min(evaluation)))).T[
                np.argsort(-evaluation)]
            self.population_index = np.asarray([int(elmt) for elmt in the_result[:, 0]])
            self.fitness = the_result[:, 1]

    def do_evaluation(self, values):
        """
        Actually perfoms the evaluation.
        """
        return values[0] if values[2] <= 0 else 0

    def frankenstein(self, population_index, the_parents, fitness):
        """
        Hacks together the genetic profile for a frankenchild from a pair of parent chromosomes.
        """
        parent1_genome = [gene.gene_state for gene in self.current_generation[population_index[the_parents[0]]].genome]
        parent2_genome = [gene.gene_state for gene in self.current_generation[population_index[the_parents[1]]].genome]
        child_genome_length = self.current_generation[int(population_index[self.inheritance(fitness[the_parents])])].genome_size
        larger_parent_genome_index = 0 if self.current_generation[population_index[the_parents[0]]].genome_size > self.current_generation[population_index[the_parents[1]]].genome_size else 1
        inheritance = [self.inheritance(fitness[the_parents]) for _ in range(min(len(parent1_genome), len(parent2_genome)))]

        frankengenome = list(np.asarray(list(zip(parent1_genome, parent2_genome)))[range(min(len(parent1_genome), len(parent2_genome))), inheritance])
        frankengenome.extend([elmt.gene_state for elmt in self.current_generation[population_index[the_parents[larger_parent_genome_index]]].genome][child_genome_length:])
        if self.unique_genes:
            frankengenome = list(set(frankengenome))
            if self.static_genome_size is not False and len(frankengenome) < len(parent1_genome):
                frankengenome.extend(list(np.random.choice([gene_state for gene_state in np.arange(len(parent1_genome)) if gene_state not in frankengenome], size=(len(parent1_genome)-len(frankengenome)), replace=1 - self.unique_genes)))
        return frankengenome

    def inheritance(self, parents):
        """
        This is the random number generator which selects the source parent for a given feature.
        """
        return 1 - (parents[0] / sum(parents) >= np.random.uniform(low=0, high=1))


class Chromosome:
    """
    The chromosome class stores a list of objects of the Gene class.
        This is stored as the genome object.

    Three flags are available to control the activity of a chromosome:
        unique_genes ensures genomes have no replicate genes | True/False
        inherited_genetics allows a specific genome to be passed to the object constructor | A list with gene states, or a False.
        static_genome_size allows the genome size to be controlled | An int or False.
    """

    def __init__(self, the_objects, unique_genes=False, inherited_genetics=False, static_genome_size=False):
        self._the_objects = the_objects
        self._n_objects = self._the_objects.shape[0]
        self.unique_genes = unique_genes
        self.static_genome_size = False if static_genome_size is False else True

        if self.static_genome_size:
            self.genome_size = static_genome_size
        elif inherited_genetics:
            self.genome_size = len(inherited_genetics)
        else:
            self.genome_size = np.random.randint(1, self._n_objects + 1)

        self.genome = [self.generate_gene(gene_state) for gene_state in self.define_genome(inherited_genetics)]

    def generate_gene(self, gene_state=False):
        """
        This is a dummy function to cleanly call the Gene constructor.
        """
        if gene_state is False:
            # if not given a specific gene to generate, contextually generates appropriate genes depending on if the unique_genes flag is True
            gene_state = \
                np.random.choice(np.arange(self._n_objects)) if self.unique_genes is False else \
                    np.random.choice([elmt for elmt in np.arange(self._n_objects) if
                                      elmt not in [gene.gene_state for gene in self.genome]])
        return Gene(self._the_objects, gene_state=gene_state)

    def define_genome(self, genetics):
        """
        This is a dummy function which modifies the genetic profile (list of gene states), given an input.
        Either passes the parent genetics to return or generates a new random genetic profile using np.random.choice.
        If the parent chromosome has the unique_genome flag, a random genetic profile cannot include repeat genes.
        """
        if not genetics:
            genetics = np.random.choice(np.arange(self._n_objects), size=self.genome_size, replace=1 - self.unique_genes)
        return genetics

    def mutate(self):
        """
        Causes the parent chromosome to mutate.
        A chromosome can mutate in three ways:
            1) Additive: A new (random) gene is appended.
            2) Subtractive: A gene (random) is removed.
            3) Edit: The gene state of one of the present genes is modified by +/- 1.
        Mutation is context sensitive:
            static_genome_size stops additive and subtractive mutations
            unique_genome stops repeats (
        """
        mutation_index = np.random.randint(-1 + self.static_genome_size, self.genome_size + 1 - self.static_genome_size)
        if mutation_index == -1:# subtractive mutation
            if self.genome_size > 1:
                self.genome_size -= 1
                del (self.genome[np.random.randint(0, self.genome_size)])
        elif mutation_index == self.genome_size:# additive mutation
            if self.unique_genes is True and mutation_index < self._n_objects - 1:
                self.genome_size += 1
                self.genome.append(self.generate_gene(np.random.choice([gene_state for gene_state in range(self._n_objects) if gene_state not in [gene.gene_state for gene in self.genome]]) if self.unique_genes is True else None))
        else:# edit mutation
            gene_modification = np.random.choice([-1, 1])

            if 0 <= (self.genome[mutation_index].gene_state + gene_modification) < self.genome_size:
                if self.unique_genes is False or self.genome[mutation_index].gene_state + gene_modification not in [gene.gene_state for gene in self.genome]:
                    self.genome[mutation_index].mutate(self.genome[mutation_index].gene_state + gene_modification)

    def get_values(self, weight_constraint):
        """
        Evaluates a chromosome.
        """
        total_value = np.sum([elmt.value for elmt in self.genome])
        total_weight = np.sum([elmt.weight for elmt in self.genome])
        overweight = total_weight - weight_constraint
        return np.asarray([total_value, total_weight, overweight])


class Gene:
    """
    The Gene class is used primarily for value storage.
        In this case, the gene state, the object value, and the object weight.

    The gene_state also doubles as a flag for when a random gene needs to be initialised.
    """

    def __init__(self, the_objects, gene_state=False):
        self._the_objects = the_objects
        self.gene_state = gene_state
        if gene_state is False:
            self.mutate()
        else:
            self._set_attributes()

    def mutate(self, gene_state=False):
        """
        There are two types of mutations:
            1) Random (if gene_state is False):
                The gene takes on a random state (index) from the list of possible objects.
            2) Specific (if self.unique_gene is False and gene_state is true):
                The new gene value is provided.
        """
        if gene_state is False:
            self.gene_state = np.random.randint(0, self._the_objects.shape[0])
        else:
            self.gene_state = gene_state
        self._set_attributes()

    def _set_attributes(self):
        """
        Stores the attributes (value and weight) as properties.
        """
        self.value = self._the_objects[self.gene_state, 0]
        self.weight = self._the_objects[self.gene_state, 1]


def main():
    filename = 'items.txt'
    items = np.loadtxt(filename)

    knapsack = Population(
        items[np.argsort(items[:, 1])],
        minimum_population_size=MINIMUM_POPULATION_SIZE,
        minimum_parents=MINIMUM_PARENTS,
        constraints=CONSTRAINTS,
        unique_genes=UNIQUE_GENES,
        static_genome_size=STATIC_GENOME_SIZE,
        mutation_rate=MUTATION_RATE)

    logging.info(
        'RUN STATS:\nminimum_population_size = %d\nminimum_parents = %d\nconstraints = %d\nunique_genes = %s\nstatic_genome_size = %s\nmutation_rate = %d\n***********' \
        % (MINIMUM_POPULATION_SIZE,
           MINIMUM_PARENTS,
           CONSTRAINTS,
           UNIQUE_GENES,
           STATIC_GENOME_SIZE,
           MUTATION_RATE))

    for i in range(GENERATIONS):
        knapsack.iterate()

    logging.info(
        'RUN RESULTS:\nValue = %d\nWeight = %d\nOverweight = %d\nContents = %s\n##########\n' \
        % (knapsack.current_generation[knapsack.population_index[0]].get_values(CONSTRAINTS)[0],
           knapsack.current_generation[knapsack.population_index[0]].get_values(CONSTRAINTS)[1],
           knapsack.current_generation[knapsack.population_index[0]].get_values(CONSTRAINTS)[2],
           [elmt.gene_state for elmt in knapsack.current_generation[knapsack.population_index[0]].genome]))

    print('Output written to logs.txt')
    # print('value|weight|overweight:', knapsack.current_generation[knapsack.population_index[0]].get_values(CONSTRAINTS))
    # print('indices of bag contents (item list was sorted by weight):', [elmt.gene_state for elmt in knapsack.current_generation[knapsack.population_index[0]].genome])


if __name__ == '__main__':
    main()
